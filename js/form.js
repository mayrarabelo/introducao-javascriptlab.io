var botaoAdicionar = document.querySelector("#adicionar-paciente");
botaoAdicionar.addEventListener("click", function(event){
  event.preventDefault();

  var form = document.querySelector("#form-adiciona");

  //extraindo informações do paciente do form
  var paciente = obtemPacienteDoFormulario(form);
  //console.log(paciente);

  //valida peso do paciente
  if (!validaPaciente(paciente)){
    alert("Peso inválido!");
    return;
  }

  //valida peso do paciente
  var erros = validaPaciente(paciente);
  //console.log(erros);//
  if (erros.length > 0){
    exibeMensagemDeErro(erros);
    return;
  }

  adicionaPacienteNaTabela(paciente);

  form.reset(); //reseta os campos do form
  var mensagenErro = document.querySelector("#mensagens-erro"); //variável para limpar o UL das mensagens de erros quando o paciente for adicionado corretamente na tabela.
  mensagenErro.innerHTML = ""; //limpar as mensagens de erro.
});

function adicionaPacienteNaTabela(paciente){
  //cria a tr e a td do paciente
 var pacienteTr = montaTr(paciente);
 //adicionando o paciente na tabela
 var tabela = document.querySelector("#tabela-pacientes");
 tabela.appendChild(pacienteTr);
}

function exibeMensagemDeErro(erros){
  var ul = document.querySelector("#mensagens-erro");
  ul.innerHTML = "";
  erros.forEach(function(erro){
    var li = document.createElement("li");
    li.textContent = erro;
    ul.appendChild(li);
  });
}

function obtemPacienteDoFormulario(form){

  var paciente ={ /*criando um objeto paciente*/
    nome: form.nome.value,
    peso: form.peso.value,
    altura: form.altura.value,
    gordura: form.gordura.value,
    imc: calculaImc(form.peso.value, form.altura.value)
  }
  return paciente;
}

function montaTr(paciente){
  var pacienteTr = document.createElement("tr"); //criar elementos, no caso criar um nova TR no HTML.
  pacienteTr.classList.add("paciente");

  /*var nomeTd = montarTd(paciente.nome, "info-nome");
  var pesoTd = montarTd(paciente.peso, "info-peso");
  var alturaTd = montarTd(paciente.altura, "info-altura");
  var gorduraTd = montarTd(paciente.gordura, "info-gordura");
  var imcTd = montarTd(paciente.imc, "info-imc");*/

  /*var nomeTd = document.createElement("td");
  var pesoTd = document.createElement("td");
  var alturaTd = document.createElement("td");
  var gorduraTd = document.createElement("td");
  var imcTd = document.createElement("td");

  nomeTd.textContent = paciente.nome;
  pesoTd.textContent = paciente.peso;
  alturaTd.textContent = paciente.altura;
  gorduraTd.textContent = paciente.gordura;
  imcTd.textContent = paciente.imc;*/

  pacienteTr.appendChild(montarTd(paciente.nome, "info-nome"));
  pacienteTr.appendChild(montarTd(paciente.peso, "info-peso"));
  pacienteTr.appendChild(montarTd(paciente.altura, "info-altura"));
  pacienteTr.appendChild(montarTd(paciente.gordura, "info-gordura"));
  pacienteTr.appendChild(montarTd(paciente.imc, "info-imc"));

  return pacienteTr;
}

function montarTd(dado, classe){
  var td = document.createElement("td");
  td.textContent = dado;
  td.classList.add(classe);
  return td;
}

function validaPaciente(paciente){
  var erros = [];

  if (paciente.nome.length == 0){
    erros.push("* O campo nome não pode ser vazio!")
  }

  if(!validaPeso(paciente.peso)){
    erros.push("* O peso é inválido!")
  }
  if(!validaAltura(paciente.altura)){
    erros.push("* A altura é inválida!")
  }
  if (paciente.gordura.length == 0){
    erros.push("* O campo gordura não pode ser vazio!")
  }
    return erros;
}
